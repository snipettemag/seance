# Seance

relay content from Medium to Ghost [WIP]

> **Note:** This app is still work in progress!



# installation

To install, run the following commands:

    cd seance
    yarn install
    yarn link

The `seance` command should now be available in your path. If not,
check permissions or run `node ./index.js` directly.

# usage

The main point of this tool is to easily pull Medium posts and add them
to a Ghost site, so that you can (for example) cross-post your content
between Medium and Ghost.

This project is created for the [Snipette](https://www.snipettemag.com)
website, and as such might have some Snipette-specific features.
However, you're free to use it as you like, and any contributions are
welcome!

## Config

Seance relies on some environment variables to work. You can either
set these globally, or place them in a file named `.env` in the same
directory where you are running Seance.

The parameters are for a WebDAV server, where Seance uploads media
files, and for your Ghost API interface. The parameters to set are:

* `WEBDAV_SERVER_URL` - location of your WebDAV server
* `WEBDAV_USERNAME` - username for signing in
* `WEBDAV_PASSWORD` - password, likewise
* `WEBDAV_PATH_PREFIX` - prefix to add to all WebDAV paths: no uploads
  will happen outside of this path
* `UPLOADED_PATH_PREFIX` - path where uploaded images will be
  served (it could end up being different from `WEBDAV_SERVER_URL`: say
  you go to `https://myhost.com:1234/dav/[folder]` to upload, but the
  public sees it as `https://media.mysite.com/[folder]`—or, more
  significantly, when you're doing a local-directory upload!
* `LOCAL_UPLOAD_PATH_PREFIX` - path where uploaded images will be copied
  locally, if you choose not to use WebDAV
* `GHOST_URL` - URL of your Ghost installation
* `GHOST_VERSION` - 'v2' or 'v3' depending on which version you're using
* `GHOST_ADMIN_KEY` - 'Admi API key for Ghost'

In case you're wondering about the WebDAV server: that's the setup we
use at Snipette. We'd like to eventually let you upload directly through
Ghost as well, but we're prioritising our setup first to get running
before we think of anything else.

Now, we've got a "local upload" option as well which basically copies
the file to a specified directory on the system. Pull requests for
anything else are welcome!

## Pull a post from Medium

    # replace the URL with an actual Medium post
    seance fetch https://medium.com/@username/some-post-abc123

This will create a new folder in `./content` containing:

* the post as a markdown file
* image files for each post (linked appropriately)
* a JSON file with post metadata

## Push a post to Ghost

    # replace some-post with name of post directory
    # seance will look in ./content/[name of post]
    seance push some-post

This will upload images to the specified WebDAV URL, and add the post
as a draft to Ghost. It will also try to auto-detect the author, and
set that if there's a match at the other end.

## Copy a Medium post directly to Ghost

    # replace the URL with an actual Medium post
    seance import https://medium.com/@username/some-post-abc123

This is basically the other two commands combined, or will be once it's
implemented. Coming soon!

## Import a Medium user to Ghost

    # Replace username and email with appropriate values
    # The username should match an existing Medium user
    seance create-user username user@example.com

This will output a JSON object. Copy-paste that object into a new file
(eg. `ghost-import.json`), then open your Ghost Admin and import the
file via the "Labs" section. This is required becaues Ghost doesn't
let you directly add users; it only lets you import them.

Seance also attempts to fetch the Medium user's profile image and upload
it via WebDAV. The JSON file will link to the WebDAV-uploaded image

# credits

* Medium import/export is inspired by and depends on a modified version
  of the [mediumexporter](https://github.com/xdamman/mediumexporter)
  library cum cli tool
* Image upload inspired by
  [ghost-webdav-storage-adapter](https://github.com/bartt/ghost-webdav-storage-adapter),
  or will be, once it's implemented

# contributing

We're developing this software internally for
[Snipette](https://www.snipettemag.com), but, if you find our tool
useful and think you can improve it, we'd be happy to have your
contributions! Just start an issue or open a pull request :)

# license

This code is released under the MIT license. See the LICENSE file for
details.
