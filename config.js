// load configuraton from .env file (if exists)
require('dotenv').config()
const convict = require('convict')
const convict_format_with_validator = require('convict-format-with-validator')
const os = require('os')
const path = require('path')
const yaml = require('js-yaml')
const fs = require('fs')

convict.addFormats(convict_format_with_validator)
convict.addParser({ extension: ['yml', 'yaml'], parse: yaml.safeLoad })

let config = convict({
  uploaded_path_prefix: {
    doc: 'URL where files are uploaded (eg. https://mysitem.com/media)',
    format: 'url',
    env: 'UPLOADED_PATH_PREFIX',
    default: null,
  },
  webdav: {
    server_url: {
      doc: 'WebDAV server URL (eg. https://myhost.com:2078)',
      format: 'url',
      env: 'WEBDAV_SERVER_URL',
      default: null,
      nullable: true,
    },
    username: {
      doc: 'Username for WebDAV server',
      format: 'String',
      env: 'WEBDAV_USERNAME',
      default: null,
      nullable: true,
    },
    password: {
      doc: 'Password for WebDAV server',
      format: 'String',
      env: 'WEBDAV_PASSWORD',
      default: null,
      sensitive: true,
      nullable: true,
    },
    path_prefix: {
      doc: 'Where to upload files (eg. /seance-uploads)',
      format: 'String',
      env: 'WEBDAV_PATH_PREFIX',
      default: null,
      nullable: true,
    },
    uploaded_path_prefix: { // FIXME: Deprecated; remove
      doc: 'URL where files are uploaded (eg. https://mysitem.com/media)',
      format: 'url',
      env: 'WEBDAV_UPLOADED_PATH_PREFIX',
      default: null,
      nullable: true,
    },
    use_digest: {
      doc: 'Whether to use digest authentication',
      format: 'Boolean',
      env: 'WEBDAV_USE_DIGEST',
      default: false,
    }
  },
  local_upload: {
     path_prefix: {
      doc: 'Where to upload files locally (eg. /media/seance-uploads)',
      format: 'String',
      env: 'LOCAL_UPLOAD_PATH_PREFIX',
      default: null,
    },
  },
  ghost: {
    url: {
      doc: 'URL of Ghost installation',
      format: 'url',
      env: 'GHOST_URL',
      default: null,
    },
    version: {
      format: 'String',
      env: 'GHOST_VERSION',
      default: 'v2',
    },
    admin_key: {
      doc: 'Admin API key for Ghost',
      format: 'String',
      env: 'GHOST_ADMIN_KEY',
      default: null,
      sensitive: true,
    }
  },
  scissors: {
    doc: 'Separator image (for comparison)',
    format: '*', // TODO: validate by checking path
    env: 'SEPARATOR_IMAGE',
    default: null,
  },
})

// Load configs from home directory, if present
userConfig = path.join(os.homedir(), '.config/seance.yaml')
if (fs.existsSync(userConfig)) {
  config.loadFile(userConfig)
} else if (process.argv[2] != 'setup') {
  console.warn(
    'Warning: no config file detected! ' +
    'Please run `seance setup` to configure your system'
  )
}

var validated

// Validate config
try {
  config.validate()
  validated = true
} catch(e) {
  console.error(e)
  validated = false
}

// Update deprecated value: config.webdav.uploaded_path_prefix
if (!!config.webdav && !!config.webdav.uploaded_path_prefix) {
  console.warn(
    'Warning: config.webdav.uploaded_path_prefix and the ' +
    'WEBDAV_UPLOADED_PATH_PREFIX environment variable are ' +
    'deprecated! Please use config.uploaded_path_prefix or ' +
    'the UPLOADED_PATH_PREFIX environment variable instead.'
  )
  config.uploaded_path_prefix = config.webdav.uploaded_path_prefix
}

allConf = config.getProperties()
allConf.validated = validated

module.exports = allConf
