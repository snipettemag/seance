function addNotification(msg, className='is-warning') {
  var n = document.createElement('div')
  n.classList.add('notification')
  n.classList.add(className)
  n.innerHTML = msg
  document.getElementById('notifications').appendChild(n)
}

// Fetching from Medium

function mediumFetch(e) {
  if(e) { e.preventDefault() }

  const postSlug = document.querySelector('[data-post-slug]').dataset.postSlug
  const mediumUrl = document.querySelector('[data-post-mediumurl]').dataset.postMediumurl
  var password = document.querySelector('input[name=password]').value
  console.log('Fetching ' + postSlug)
  const socketProtocol = (window.location.protocol === 'https:' ? 'wss:' : 'ws:')
  const socketUrl = socketProtocol + '//' + window.location.host + '/ws/fetch-medium/'
  const socket = new WebSocket(socketUrl)

  // set up the div
  statusDiv = document.getElementById('statusbar')
  statusText = document.createElement('p')
  statusText.innerText = 'Fetching post'
  statusText.classList.add('card-footer-item')

  // hide buttons
  for (i=0;i<statusDiv.children.length;i++) {
    statusDiv.children[i].classList.add('is-hidden')
  }

  statusDiv.appendChild(statusText)

  socket.onopen = () => {
    socket.send('fetch ' + postSlug + ' ' + password)

    // keepAlive
    socket.send('__ping__')
  }

  socket.onmessage = m => {
    console.log('Got message: ' + m.data)

    // process keepAlive
    if (m.data == '__pong__') {
      setTimeout(()=>{socket.send('__ping__')}, 2000)
      return
    }

    if (m.data.indexOf(': ') != -1) {
      var command = m.data.slice(0, m.data.indexOf(': '))
      var commandData = m.data.slice(m.data.indexOf(': ')+2)

      statusText.innerHTML = `<b>${command}: </b>${commandData}`

      if (command == 'notification') {
        addNotification(commandData)
      } else if (command == 'done' || command == 'error') {

        // if it's an error, say it
        if (command == 'error') {
          addNotification(commandData, className='is-danger')
        } else {
          // show alert
          addNotification(`${postSlug} has been fetched successfully. You can now push it to Ghost`, 'is-success')
        }

        // since it's done, clean up
        // remove statusbar
        statusDiv.removeChild(statusText)
        socket.close()


        // show buttons, change text and save slug
        for (i=0;i<statusDiv.children.length;i++) {
          if (statusDiv.children[i].innerText.startsWith('Fetch')) {
            statusDiv.children[i].innerText = 'Fetch again'
          }
          if (statusDiv.children[i].innerText == 'Push to Ghost') {
            statusDiv.children[i].dataset.postSlug = commandData
          }
          statusDiv.children[i].classList.remove('is-hidden')
        }
      }

    } else {
      statusText.innerHTML = m.data
    }
  }
}


// Pushing to Ghost

function ghostPush(e) {
  if(e) { e.preventDefault() }

  let postSlug = document.querySelector('[data-post-slug]').dataset.postSlug
  var password = document.querySelector('input[name=password]').value
  console.log('Pushing ' + postSlug)
  const socketProtocol = (window.location.protocol === 'https:' ? 'wss:' : 'ws:')
  const socketUrl = socketProtocol + '//' + window.location.host + '/ws/push-ghost/'
  const socket = new WebSocket(socketUrl)

  // set up the div
  statusDiv = document.getElementById('statusbar')
  statusText = document.createElement('p')
  statusText.innerText = 'Pushing to Ghost'
  statusText.classList.add('card-footer-item')

  // hide buttons
  for (i=0;i<statusDiv.children.length;i++) {
    statusDiv.children[i].classList.add('is-hidden')
  }

  statusDiv.appendChild(statusText)

  socket.onopen = () => {
    socket.send('push ' + postSlug + ' ' + password)

    // keepAlive
    socket.send('__ping__')
  }

  socket.onmessage = m => {
    console.log('Got message: ' + m.data)

    // process keepAlive
    if (m.data == '__pong__') {
      setTimeout(()=>{socket.send('__ping__')}, 2000)
      return
    }

    if (m.data.indexOf(': ') != -1) {
      var command = m.data.slice(0, m.data.indexOf(': '))
      var commandData = m.data.slice(m.data.indexOf(': ')+2)

      statusText.innerHTML = `<b>${command}: </b>${commandData}`

      if (command == 'notification') {
        addNotification(commandData)
      } else if (command == 'done' || command == 'error') {

        // if it's an error, say it
        if (command == 'error') {
          addNotification(commandData, className='is-danger')
        } else {
          // show alert
          // TODO: don't hard-code destination URL
          // (instead, fetch from server's config and display)

          // first, unpack info
          let postEditUrl

          commandData = commandData.split(' ')
          if (!!commandData[0]) postSlug = commandData[0]
          if (!!commandData[1]) postEditUrl = commandData[1]

          let msg = `${postSlug} has been pushed to Ghost. Buffer share link: <a href="https://www.snipettemag.com/${postSlug}" target="_blank">https://www.snipettemag.com/${postSlug}</a>`

          if (!!postEditUrl) {
            msg = msg + `. <a href="${postEditUrl}" class="button" target="_blank">Edit on Ghost</a>`
          }

          addNotification(msg, 'is-success')
        }

        // since it's done, clean up
        // remove statusbar
        statusDiv.removeChild(statusText)
        socket.close()

        // show buttons and change text
        for (i=0;i<statusDiv.children.length;i++) {
          if (statusDiv.children[i].innerText.startsWith('Push')) {
            statusDiv.children[i].innerText = 'Push again'
          }
          statusDiv.children[i].classList.remove('is-hidden')
        }
      }

    } else {
      statusText.innerHTML = m.data
    }
  }
}
